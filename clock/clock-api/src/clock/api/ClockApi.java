package clock.api;

public interface ClockApi {

    String getCurrentTime();
}
