module clock.service {

    requires clock.api;

    provides clock.api.ClockApi with clock.service.ClockService;

}