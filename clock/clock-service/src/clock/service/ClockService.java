package clock.service;

import clock.api.ClockApi;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class ClockService implements ClockApi {
    @Override
    public String getCurrentTime() {
        return LocalTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME);
    }
}
