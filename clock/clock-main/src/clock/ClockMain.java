package clock;


import clock.api.ClockApi;

import java.util.ServiceLoader;

public class ClockMain {

    public static void main(String[] args) {

        clock.api.ClockApi clockApi = ServiceLoader.load(ClockApi.class).findFirst()
                        .orElseThrow();
        System.out.println("current time: " + clockApi.getCurrentTime());
    }
}
