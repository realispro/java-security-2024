<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
<header>
    <title>Librarian</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<c:url value = "/css/style.css"/>"/>
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="./js/script.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet"/>
</header>

<body>
<script>window.ctx = "${pageContext.request.contextPath}"</script>
<header>
    <a href="./publishers"><h1>Librarian</h1></a>
    <h2><%=request.getAttribute("title")%>></h2>
    <h3>
        <% if(request.getUserPrincipal()!=null){%>
    Logged in as [<%=request.getUserPrincipal().getName()%>]
        <%}%>
    </h3>
</header>
<section>

