<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="./header.jsp" flush="true"/>


    <table class="form">
        <form action="./addBook" method="post">
            <tr>
                <td>Title:</td><td><input type="text" name="title"/></td>
            </tr>
            <tr>
                <td>Author:</td><td><input type="text" name="author"/></td>
            </tr>
            <tr>
                <td>Price:</td><td><input name="price"/></td>
            </tr>
            <tr>
                <td>Cover:</td><td><input name="cover"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="hidden" name="publisherId" value="${publisherId}">
                    <input type="hidden" name="csrfToken" value="${sessionScope.csrfToken}">
                    <input type="submit" value="Add">
                </td>
            </tr>
        </form>
    </table>

<jsp:include page="./footer.jsp" flush="true"/>
