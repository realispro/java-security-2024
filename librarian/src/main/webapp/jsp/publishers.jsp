<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>


<jsp:include page="./header.jsp" flush="true"/>

            <table>
                <tr>
                    <th>Logo</th>
                    <th>Name</th>
                </tr>
                <c:forEach var="p" items="${publishers}">
                    <tr>
                        <td>
                            <img src="${p.logoImage}"/>
                        </td>
                        <td>
                            <a href="./books?publisherId=${p.id}">${p.name}</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>

<jsp:include page="./footer.jsp" flush="true"/>

