<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="./header.jsp" flush="true"/>


    <table>
            <tr>
                <td>Title:</td><td>${book.title}</td>
            </tr>
            <tr>
                <td>Author:</td><td>${book.author}</td>
            </tr>
            <tr>
                <td>Price:</td><td>${book.price}</td>
            </tr>
            <tr>
                <td>Cover:</td><td><img src="${book.cover}"/></td>
            </tr>
    </table>

<jsp:include page="./footer.jsp" flush="true"/>
