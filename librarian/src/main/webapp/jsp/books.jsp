<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="./header.jsp" flush="true"/>

            <table>
                <tr>
                    <th>Cover</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Price</th>
                    <th>Add to order</th>
                </tr>
                <c:forEach var="b" items="${books}">
                    <tr>
                        <td>
                            <img src="${b.cover}"/>
                        </td>
                        <td>
                            <a href="./book?bookId=${b.id}"><c:out value="${b.title}"/></a>
                        </td>
                        <td>
                            ${b.author}
                        </td>
                        <td>
                            ${b.price}
                        </td>
                        <td>
                            <p onclick="javascript:addToOrder(${b.id})" class="action">+</p>
                        </td>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="5">
                        <a href="./addBook?publisherId=${publisherId}">Add new</a>
                    </td>
                </tr>

            </table>

            <jsp:include page="./footer.jsp" flush="true"/>
