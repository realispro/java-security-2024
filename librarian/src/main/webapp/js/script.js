$(document).ready(function(){

    $("#emptyItems").click(function(){alert('clearing ...');resetOrder();})
    $("#submitItems").click(function(){alert('submitting ...');submitOrder();})
    getBooks();

});

var serverUri = window.location.origin + "/librarian/webapi";


function getBooks(){
     $.ajax({
          method: "GET",
          url: serverUri + "/orderitems",
          Accept : "application/json",
          contentType: "application/json"
     }).done(function(books, status){
        $("#orders table > tbody > tr").remove();
        var totalPrice = 0;
        books.forEach(function(book){
            $("#orders table")
                .append("<tr><td>" + book.name + "</td><td>" +
                 book.price + "</td><td>" + book.title +
                 "</td><td onclick='javascript:removeBook(" + book.id + ")'><p class='action'>-</p></td></tr>");
            totalPrice = totalPrice + book.price;
        });
        $("#totalPrice > span").text(totalPrice);
    } );
}

function addToOrder(id){
    $.ajax({
      method: "POST",
      url: serverUri + "/orderitems/"+id
    }).done(function() {
        getBooks();
    });
}

function removeBook(id){
    $.ajax({
      method: "DELETE",
      url: serverUri + "/orderitems/" + id
    }).done(function() {
        getBooks();
    });
}

function resetOrder(){
     $.ajax({
       method: "DELETE",
       url: serverUri + "/orderitems"
     }).done(function() {
         getBooks();
     });
 }

 function submitOrder(){
     $.ajax({
       method: "POST",
       url: serverUri + "/orderitems"
     }).done(function() {
         getBooks();
         alert("The order has been submited. Thank You!");
     });
 }