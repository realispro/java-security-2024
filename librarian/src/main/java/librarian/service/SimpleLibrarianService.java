package librarian.service;


import jakarta.inject.Inject;
import librarian.api.LibrarianService;
import librarian.dao.BooksDAO;
import librarian.model.Book;
import librarian.model.Publisher;

import java.util.List;

public class SimpleLibrarianService implements LibrarianService {

    @Inject
    private BooksDAO booksDAO;

    public List<Publisher> getPublishers() {
        return booksDAO.getAllPublishers();
    }

    public Publisher getPublisher(Long id){
        return booksDAO.getPublisherById(id);
    }

    public List<Book> getBooksForPublisher(Long rId) {
        Publisher r = getPublisher(rId);
        return booksDAO.getBooksByPublisher(r);
    }

    public Book getBookById(Long mId) {
        return booksDAO.getBookById(mId);
    }

    public Publisher addPublisher(Publisher p) {
        throw new UnsupportedOperationException("not implemented here");
    }

    public Book addBook(Publisher p, Book b) {
        b.setPublisher(p);
        return booksDAO.addBook(b);
    }

}
