package librarian.web.ws;

import com.google.gson.Gson;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import librarian.dao.BooksDAO;
import librarian.model.Book;
import librarian.model.Order;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

@Path("/orderitems")
@Produces("*/*")
@Consumes("*/*")
public class OrderController implements Serializable{

    private Logger logger = Logger.getLogger(OrderController.class.getName());

    private static Order o = new Order();

    @Inject
    BooksDAO dao;

    @GET
    @Produces("application/json")
    public Set<BookData> getBooks(){
        logger.info("obtaining ordered book list");
        Set<BookData> mds = new HashSet<>();
        for(Book m: o.getBooks()){
            mds.add(new BookData(m.getId(), m.getPrice(), m.getTitle(), m.getPublisher().getName()));
        }
        logger.info("returning list of " +mds.size() + " ordered books");
        return mds;
    }

    @POST
    @Path("/{id}")
    public void add(@PathParam("id") long mId){
        logger.info("adding book " + mId + " to order");
        o.addBook(dao.getBookById(mId));
    }

    @DELETE
    @Path("/{id}")
    public void remove(@PathParam("id") Long mId){
        logger.info("removing book " + mId + " to order");
        o.removeBook(dao.getBookById(mId));
    }

    @DELETE
    public String clear(){
        logger.info("reseting order");
        o.reset();
        return "success";
    }

    @POST
    public String submit(){
        logger.info("finalizing order: " + new Gson().toJson(o));
        o.reset();
        return "success";
    }

    public static class BookData {
        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRestaurantName() {
            return restaurantName;
        }

        public void setRestaurantName(String restaurantName) {
            this.restaurantName = restaurantName;
        }

        public BookData(Long id, Integer price, String name, String restaurantName) {
            this.id = id;
            this.price = price;
            this.name = name;
            this.restaurantName = restaurantName;
        }

        private Long id;
        private Integer price;
        private String name;
        private String restaurantName;
    }
}
