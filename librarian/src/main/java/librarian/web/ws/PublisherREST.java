package librarian.web.ws;

import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.security.enterprise.SecurityContext;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import librarian.api.LibrarianService;
import librarian.model.Publisher;

import java.util.List;
import java.util.logging.Logger;

@Path("/")
@Produces("application/json")
@Consumes("*/*")
public class PublisherREST {

    private static final Logger logger = Logger.getLogger(PublisherREST.class.getName());

    @Inject
    private LibrarianService service;

    @Inject
    private SecurityContext securityContext;

    @GET
    @Path("/publishers")
    @RolesAllowed({"admin"})
    public List<Publisher> getPublishers(){
        logger.info("about to get publishers");
        logger.info("principal name: " + securityContext.getCallerPrincipal().getName());
        logger.info("has admin role: " + securityContext.isCallerInRole("admin"));
        logger.info("has user role: " + securityContext.isCallerInRole("user"));
        return service.getPublishers();
    }
}
