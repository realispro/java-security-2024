package librarian.web.servlet;

import jakarta.inject.Inject;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import librarian.api.LibrarianService;
import librarian.model.Book;
import librarian.model.Publisher;

import java.io.IOException;
import java.util.logging.Logger;

public class BookServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(BooksServlet.class.getName());

    @Inject
    LibrarianService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String bookId = req.getParameter("bookId");

        Book book = service.getBookById(Long.parseLong(bookId));
        if(book==null){
            logger.severe("there is no book with id " + bookId);
        } else {
            req.setAttribute("book", book);
            req.setAttribute("title", "Details of a book '" + book.getTitle() + "'");
            req.getRequestDispatcher("/jsp/book.jsp").forward(req, resp);
        }
    }
}
