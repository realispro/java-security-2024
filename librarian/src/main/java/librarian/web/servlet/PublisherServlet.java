package librarian.web.servlet;

import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import librarian.api.LibrarianService;
import librarian.model.Publisher;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

@WebServlet("/publishers")
public class PublisherServlet extends HttpServlet {

    Logger logger = Logger.getLogger(PublisherServlet.class.getName());

    @Inject
    LibrarianService service;

    @Override
    @RolesAllowed({"abc"})
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        logger.info("about to fetch publishers list");

        List<Publisher> publishers = service.getPublishers();
        req.setAttribute("publishers", publishers);
        req.setAttribute("title", "Publishers list");

        logger.fine("found " + publishers.size() + " items");

        req.getRequestDispatcher("/jsp/publishers.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("post request received ");

        try(BufferedReader br = req.getReader()){
            br.lines().forEach(logger::info);
        }
    }
}
