package librarian.web.servlet;

import jakarta.inject.Inject;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import librarian.api.LibrarianService;
import librarian.model.Publisher;


import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Logger;

public class BooksServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(BooksServlet.class.getName());

    @Inject
    LibrarianService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String pId = req.getParameter("publisherId");

        Publisher p = service.getPublisher(Long.parseLong(pId));
        if(p==null){
            logger.severe("there is no publisher with id " + pId);
        } else {
            req.setAttribute("books", service.getBooksForPublisher(p.getId()));
            req.setAttribute("title", "Book list of publisher '" + p.getName() + "'");
            req.setAttribute("publisherId", p.getId());
            req.getRequestDispatcher("/jsp/books.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("post request received ");

        try(BufferedReader br = req.getReader()){
            br.lines().forEach(logger::info);
        }
    }
}
