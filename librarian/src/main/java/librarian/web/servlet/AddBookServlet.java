package librarian.web.servlet;

import jakarta.inject.Inject;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import librarian.api.LibrarianService;
import librarian.model.Book;
import librarian.model.Publisher;

import java.io.BufferedReader;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.logging.Logger;

@WebServlet("/addBook")
public class AddBookServlet extends HttpServlet {

    Logger logger = Logger.getLogger(AddBookServlet.class.getName());

    @Inject
    LibrarianService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String pId = req.getParameter("publisherId");
        logger.info("preparing adding book form for a publisher " + pId);
        Publisher p = service.getPublisher(Long.parseLong(pId));

        req.setAttribute("publisherId", pId);
        req.setAttribute("title", "Adding new book of publisher '" + p.getName() + "'");

        HttpSession session = req.getSession(true);
        String csrfToken = generateCsrfToken();
        session.setAttribute("csrfToken", csrfToken);



        req.getRequestDispatcher("/jsp/addBook.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("post request received ");

        // csrf
        String sessionCsrfToken = (String) req.getSession().getAttribute("csrfToken");
        String requestCsrfToken = req.getParameter("csrfToken");

        if (sessionCsrfToken != null && requestCsrfToken != null && sessionCsrfToken.equals(requestCsrfToken)) {
            Book book = populateBook(req);
            long publisherId = Long.parseLong(req.getParameter("publisherId"));
            Publisher publisher = service.getPublisher(publisherId);

            book = service.addBook(publisher, book);
            req.setAttribute("book", book);
            req.setAttribute("title", "Details of a book '" + book.getTitle() + "'");

            req.getRequestDispatcher("/jsp/book.jsp").forward(req, resp);
            req.getSession().removeAttribute("csrfToken");
        } else {
            resp.setStatus(403);
        }



    }

    private Book populateBook(HttpServletRequest request){
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        int price = Integer.parseInt(request.getParameter("price"));
        String cover = request.getParameter("cover");
        Book book = new Book();
        book.setTitle(title);
        book.setAuthor(author);
        book.setPrice(price);
        book.setCover(cover);
        return book;
    }

    private String generateCsrfToken() {
        byte[] bytes = new byte[16];
        new SecureRandom().nextBytes(bytes);
        return Base64.getEncoder().encodeToString(bytes);
    }
}
