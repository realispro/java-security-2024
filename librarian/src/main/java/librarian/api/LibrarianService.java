package librarian.api;

import librarian.model.Book;
import librarian.model.Publisher;

import java.util.List;

public interface LibrarianService {

    List<Publisher> getPublishers();

    public Publisher getPublisher(Long id);

    List<Book> getBooksForPublisher(Long rId);

    Book getBookById(Long mId);

    Publisher addPublisher(Publisher r);

    Book addBook(Publisher r, Book m);
}
