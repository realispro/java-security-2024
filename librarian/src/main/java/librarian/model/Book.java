package librarian.model;

import java.io.Serializable;


public class Book implements Serializable{

    private Long id;

    private String title;

    private String author;

    private String cover;

    private int price;

    private Publisher publisher;

    public Book(Long id, String title, String author, String cover, int price, Publisher servedIn) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.cover = cover;
        this.price = price;
        this.publisher = servedIn;
        servedIn.getBooks().add(this);
    }

    public Book(){}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", publisher=" + publisher +
                '}';
    }

}
