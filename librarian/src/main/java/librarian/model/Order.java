package librarian.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;



public class Order implements Serializable {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Set<Book> books = new HashSet<Book>();

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    public void addBook(Book m) {
        books.add(m);
    }

    public void removeBook(Book m) {
        books.remove(m);
    }

    public void reset() {
        books.clear();
    }

    @Override
    public String toString() {
        return "Order{" +
                "books=" + books +
                '}';
    }
}
