package librarian.dao;

import librarian.model.Book;
import librarian.model.Publisher;

import java.util.List;


public interface BooksDAO {

    List<Publisher> getAllPublishers();

    Publisher getPublisherById(Long id);

    List<Book> getBooksByPublisher(Publisher r);

    Book getBookById(Long mId);

    Publisher addPublisher(Publisher r);

    Book addBook(Book m);

}
