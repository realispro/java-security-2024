package librarian.dao.jdbc;

import jakarta.annotation.Resource;
import librarian.dao.BooksDAO;
import librarian.model.Book;
import librarian.model.Publisher;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JdbcBooksDAO implements BooksDAO {

    public static final Logger logger = Logger.getLogger(JdbcBooksDAO.class.getName());

    public static final String SELECT_ALL_PUBLISHERS = "select p.id as publisher_id, p.logoimage as publisher_logo, " +
            "p.name as publisher_name from publisher p";

    public static final String SELECT_PUBLISHER_BY_ID = "select p.id as publisher_id, p.logoimage as publisher_logo, " +
            "p.name as publisher_name from publisher p where id=?";

    public static final String SELECT_BOOK_BY_ID = "select b.id as book_id, " +
            "b.title as book_title, b.author as book_author, b.cover as book_cover, b.price as book_price," +
            "p.id as publisher_id, p.logoimage as publisher_logo, " +
            "p.name as publisher_name " +
            "from book b, publisher p where b.publisher_id = p.id and b.id = ?";
    public static final String SELECT_BOOKS_BY_PUBLISHER =  "select b.id as book_id, " +
            "b.title as book_title, b.author as book_author, b.cover as book_cover,  b.price as book_price," +
            "p.id as publisher_id, p.logoimage as publisher_logo,  " +
            "p.name as publisher_name  " +
            "from book b, publisher p where b.publisher_id = p.id and p.id=?";


    public static final String INSERT_PUBLISHER = "insert into publisher " +
            "(id,name,logoimage) values(?, ?, ?)";


    public static final String INSERT_PUBLISHER_BOOK = "insert into book " +
            "(title,author,cover,price,publisher_id) values(?, ?, ?, ?, ?)";

    @Resource( lookup = "java:jboss/datasources/LibrarianDS")
    private DataSource dataSource;

    public List<Publisher> getAllPublishers() {
        List<Publisher> publishers = new ArrayList<Publisher>();
        try(Connection con = this.dataSource.getConnection();
            Statement statement = con.createStatement();) {
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_PUBLISHERS);
            while (resultSet.next()) {
                publishers.add(mapPublisher(resultSet));
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return publishers;
    }

    public Publisher getPublisherById(Long id) {
        Publisher r = null;
        try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(SELECT_PUBLISHER_BY_ID);) {
            prpstm.setLong(1, id);
            ResultSet rs = prpstm.executeQuery();
            if(rs.next()) {
                r = mapPublisher(rs);
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return r;
    }

    public List<Book> getBooksByPublisher(Publisher r) {
        List<Book> books = new ArrayList<>();
        try (Connection con = this.dataSource.getConnection();
             PreparedStatement prpstm = con.prepareStatement(SELECT_BOOKS_BY_PUBLISHER);) {
            prpstm.setLong(1, r.getId());
            ResultSet rs = prpstm.executeQuery();
            while(rs.next()){
                books.add(mapBook(rs, r));
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return books;
    }

    public Book getBookById(Long mId) {
        Book book = null;
        try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(SELECT_BOOK_BY_ID);) {
            prpstm.setLong(1, mId);
            ResultSet rs = prpstm.executeQuery();
            if(rs.next()){
                book = mapBook(rs, null);
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return book;
    }

    public Publisher addPublisher(Publisher p) {
        try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(INSERT_PUBLISHER);) {
            prpstm.setLong(1, p.getId());
            prpstm.setString(2,p.getName());
            prpstm.setString(3,p.getLogoImage());
            prpstm.executeUpdate();

            p = getPublisherById(p.getId());

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return p;
    }

    public Book addBook(Book b) {

        try(Connection con = this.dataSource.getConnection();PreparedStatement statement = con.prepareStatement(INSERT_PUBLISHER_BOOK)){
          /*  String sql = "insert into book (author,cover,price,publisher_id, title) values("
                    + "'" + b.getAuthor() + "',"
                    + "'" + b.getCover() + "',"
                    + b.getPrice() + ","
                    + b.getPublisher().getId() + ","
                    + "'" + b.getTitle() + "');";*/
            statement.setString(1, b.getTitle());
            statement.setString(2, b.getAuthor());
            statement.setString(3, b.getCover());
            statement.setInt(4, b.getPrice());
            statement.setLong(5, b.getPublisher().getId());

            logger.info("about to execute a command: " + statement.toString());
            int updated = statement.executeUpdate();

                    //b = getBookById(b.getId());

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return b;
    }

    private Publisher mapPublisher(ResultSet rs) throws SQLException {
        Publisher r = new Publisher();
        r.setId(rs.getLong("publisher_id"));
        r.setName(rs.getString("publisher_name"));
        r.setLogoImage(rs.getString("publisher_logo"));
        return r;
    }

    private Book mapBook(ResultSet rs, Publisher r) throws SQLException {
        Book m = new Book();
        m.setId(rs.getLong("book_id"));
        m.setTitle(rs.getString("book_title"));
        m.setAuthor(rs.getString("book_author"));
        m.setCover(rs.getString("book_cover"));
        m.setPrice(rs.getInt("book_price"));
        if(r==null){
            r = mapPublisher(rs);
        }
        m.setPublisher(r);

        return m;
    }

}



