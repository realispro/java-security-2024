package lab.shopping;

public class CreditCard implements Payment{


    private String number;

    private int ccv;


    public CreditCard() {
    }

    public CreditCard(String number, int ccv) {
        this.number = number;
        this.ccv = ccv;
    }

   public void chargeCard(double value) {
        System.out.println("charging card " + number);
    }

    @Override
    public void pay(double value) {
        chargeCard(value);
    }
}
