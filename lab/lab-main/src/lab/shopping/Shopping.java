package lab.shopping;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


public class Shopping {

    private final Key key;
    private List<Item> items = new ArrayList<Item>();

    public Shopping(Key key) {
        this.key = key;
    }

    public void addItem(Item i) {
        items.add(i);
    }

    private String getInvoice(double price) {
        return String.format("Invoice at %s, for %s, total price %f",
                LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME),
                items.toString(),
                price);
    }

    public byte[] pay(Payment payment) {

        if (payment != null) {
            double price = calcPrice();
            payment.pay(price);
            try {

                Cipher cipher = Cipher.getInstance("AES");
                cipher.init(Cipher.ENCRYPT_MODE, key);

                return cipher.doFinal(getInvoice(price).getBytes(StandardCharsets.UTF_8));
            }catch (NoSuchAlgorithmException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException |
                    NoSuchPaddingException e ){
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("no payment method available");
        }
    }

    private double calcPrice() {
        double price = 0;
        for (Item i : items) {
            price += (i.getPrice() * i.getAmount());
        }
        return price;
    }

}
