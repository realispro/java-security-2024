package lab.shopping;


import javax.crypto.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class ShoppingMain {

    public static void main(String[] args) throws NoSuchAlgorithmException {
        System.out.println("let's buy smth!");

        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(128, new SecureRandom());
        Key key = keyGenerator.generateKey();
        Shopping s = new Shopping(key);
        s.addItem(new Item("mleko", 1, 4.3));
        s.addItem(new Item("bulki", 10, 1.1));
        s.addItem(new Item("paluszki", 2, 2.79));

        Payment payment = createPaymentInstance();
        // price -> System.out.printf("Deducting amount %f\n", price);
        byte[] encryptedInvoiceBytes = s.pay(payment);

        System.out.println("Invoice received: " + decryptInvoice(encryptedInvoiceBytes, key));
    }

    private static String decryptInvoice(byte[] encryptedInvoice, Key key){
        try {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decryptedInvoice = cipher.doFinal(encryptedInvoice);
            return new String(decryptedInvoice, StandardCharsets.UTF_8);
        }catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException |
                BadPaddingException e){
            throw new RuntimeException(e);
        }

    }

    private static Payment createPaymentInstance() {
        try {
            URL paymentLibURL =
                    new URL("file:///C:/lab/ws/java-security-2024/out/artifacts/blik_payment_jar/blik-payment.jar");
            ClassLoader paymentClassLoader = new URLClassLoader(new URL[]{paymentLibURL});

            String paymentClassName = getPaymentClassName(paymentClassLoader);

            //ShoppingMain.class.getClassLoader();

            Class<?> paymentClass = paymentClassLoader.loadClass(paymentClassName);
            Constructor constructor = paymentClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            Object paymentObject = constructor.newInstance();
            if (paymentObject instanceof Payment payment) {
                return payment;
            }
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException |
                 InvocationTargetException | MalformedURLException e) {
            throw new RuntimeException(e);
        }
        throw new IllegalArgumentException("smth got wrong :(");
    }

    private static String getPaymentClassName(ClassLoader classLoader) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                classLoader.getResourceAsStream("lab/shopping/payment.txt")))) {
            return br.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}