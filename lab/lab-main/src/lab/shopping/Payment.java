package lab.shopping;

public interface Payment {

    void pay(double value);
}
