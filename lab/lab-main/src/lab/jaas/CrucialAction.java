package lab.jaas;

import java.security.PrivilegedAction;

public class CrucialAction implements PrivilegedAction<Boolean> {
    @Override
    public Boolean run() {

        SecurityManager securityManager = System.getSecurityManager();
        if(securityManager!=null){
            securityManager.checkPermission(new RuntimePermission("payment"));
        }
        System.out.println("Performing payment action");
        return true;
    }
}
