package lab.jaas;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

public class LoginService {

    public Subject login() throws LoginException {
        LoginContext loginContext = new LoginContext( "jaasApp", new ConsoleCallbackHandler());
        loginContext.login();
        return loginContext.getSubject();
    }
}
