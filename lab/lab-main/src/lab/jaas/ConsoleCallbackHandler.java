package lab.jaas;

import javax.security.auth.callback.*;
import java.io.IOException;
import java.util.Scanner;

public class ConsoleCallbackHandler implements CallbackHandler {

    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        //Console console = System.console();

        Scanner scanner = new Scanner(System.in);
        for (Callback callback : callbacks) {
            if (callback instanceof NameCallback) {
                NameCallback nameCallback = (NameCallback) callback;
                nameCallback.setName(scanner.nextLine());
            } else if (callback instanceof PasswordCallback) {
                PasswordCallback passwordCallback = (PasswordCallback) callback;
                passwordCallback.setPassword(scanner.nextLine().toCharArray());
            } else {
                throw new UnsupportedCallbackException(callback);
            }
        }
    }
}
