package lab.jaas;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginException;

public class JaasMain {

    public static void main(String[] args) throws LoginException {

        System.setSecurityManager(new SecurityManager());
        LoginService loginService = new LoginService();
        Subject subject = loginService.login();
        System.out.println("Subject: " + subject.getPrincipals());

        Subject.doAsPrivileged(subject, new CrucialAction(), null);
    }
}
