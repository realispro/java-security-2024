package lab.crypto;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

public class KeyMain {

    public static void main(String[] args) throws NoSuchAlgorithmException {

        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");

        keyGenerator.init(128, new SecureRandom());
        Key key = keyGenerator.generateKey();

        String encodedKey = Base64.getEncoder().encodeToString(key.getEncoded());


        byte[] encodedKeyBytes = Base64.getDecoder().decode(encodedKey);

        Key keyDuplicate = new SecretKeySpec(encodedKeyBytes, "AES");

        System.out.println("same keys? " + key.equals(keyDuplicate) + ", " + encodedKey);

    }
}
