package lab.crypto;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

public class SignatureMain {

    public static void main(String[] args) throws KeyStoreException, IOException, UnrecoverableKeyException, NoSuchAlgorithmException, SignatureException, InvalidKeyException, CertificateException {

        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(new FileInputStream("./lab/lab-main/src/lab-keystore.jks"), "changeit".toCharArray());

        Certificate certificate = keyStore.getCertificate("acme");

        PublicKey publicKey = certificate.getPublicKey();
        PrivateKey privateKey = (PrivateKey) keyStore.getKey("acme", "changeit".toCharArray());

        String document = "Very important document to be signed";
        byte[] sign = signDocument(privateKey, document);

        System.out.println("sign is valid? " + verifySignature(publicKey, document, sign));
    }

    private static byte[] signDocument(PrivateKey key, String document) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(key);
        signature.update(document.getBytes(StandardCharsets.UTF_8));
        return signature.sign();
    }

    private static boolean verifySignature(PublicKey key, String document, byte[] sign){

        try {
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initVerify(key);
            signature.update(document.getBytes(StandardCharsets.UTF_8));
            return signature.verify(sign);
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            throw new RuntimeException(e);
        }
    }


}
