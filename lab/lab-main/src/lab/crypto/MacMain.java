package lab.crypto;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class MacMain {

    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException {

        Key key = KeyGenerator.getInstance("HmacSHA512").generateKey();

        String text = "It's almost a weekend :)";
        byte[] macValue = calculateMacValue(key, text);

        //text = text.substring(0, text.length()-2) + "(";
        byte[] confirmationVacValue = calculateMacValue(key, text);

        System.out.println("mac confirmed? " + Arrays.equals(macValue, confirmationVacValue));
    }

    private static byte[] calculateMacValue(Key key, String text) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(key);
        mac.update(text.getBytes(StandardCharsets.UTF_8));
        return mac.doFinal();
    }
}
