package lab.crypto;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

public class DigestMain {

    public static void main(String[] args) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        
        String text = "Digest It!";
        
        md.update(text.getBytes(StandardCharsets.UTF_8));
        
        byte[] digest = md.digest();

        String digestString = Base64.getEncoder().encodeToString(digest);
        System.out.println("digestString = " + digestString);

        byte[] digest2 = Base64.getDecoder().decode(digestString);

        System.out.println(Arrays.equals(digest, digest2));
        
        
    }
}
