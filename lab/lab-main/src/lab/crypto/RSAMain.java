package lab.crypto;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Base64;

public class RSAMain {

    public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, KeyStoreException,
            IOException, CertificateException, UnrecoverableKeyException {

        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(new FileInputStream("./lab/lab-main/src/lab-keystore.jks"), "changeit".toCharArray());

        Certificate certificate = keyStore.getCertificate("acme");

        PublicKey publicKey = certificate.getPublicKey();
        PrivateKey privateKey = (PrivateKey) keyStore.getKey("acme", "changeit".toCharArray());

        String plainText = "very crucial and secret content";

        // encryption
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encryptedBytes = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));

        String encryptedText = Base64.getEncoder().encodeToString(encryptedBytes);
        System.out.println("encryptedText = " + encryptedText);

        // decryption
        Cipher decryptionCipher = Cipher.getInstance("RSA");
        decryptionCipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedBytes = decryptionCipher.doFinal(encryptedBytes);

        System.out.println("decrypted text = " + new String(decryptedBytes, StandardCharsets.UTF_8));




    }
}
