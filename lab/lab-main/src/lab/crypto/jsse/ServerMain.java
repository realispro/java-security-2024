package lab.crypto.jsse;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

public class ServerMain {

    public static void main(String[] args) throws KeyStoreException, NoSuchAlgorithmException, IOException, UnrecoverableKeyException, CertificateException, KeyManagementException {

        //ServerSocket ss = new ServerSocket( 8888 );
        SSLContext sslContext = SSLContextProvider.provideSSLContext(
                "./lab/lab-main/src/server-truststore.jks", "changeit",
                "./lab/lab-main/src/lab-keystore.jks", "changeit", "changeit"
        );
        SSLServerSocketFactory ssf = sslContext.getServerSocketFactory();
        SSLServerSocket ss = (SSLServerSocket) ssf.createServerSocket(8888);
        ss.setNeedClientAuth(true);

        while (true) {
            Socket socket = ss.accept();
            try(Writer writer = new OutputStreamWriter(socket.getOutputStream())){
                System.out.println("client connected: " + socket.getInetAddress());
                writer.append("good luck and good bye :)");
            }
        }
    }


}
