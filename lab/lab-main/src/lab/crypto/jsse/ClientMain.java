package lab.crypto.jsse;

import javax.net.ssl.SSLContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

public class ClientMain {

    public static void main(String[] args) throws KeyStoreException, NoSuchAlgorithmException, IOException, UnrecoverableKeyException, CertificateException, KeyManagementException {


        SSLContext sslContext = SSLContextProvider.provideSSLContext(
                "./lab/lab-main/src/client-truststore.jks", "changeit",
                "./lab/lab-main/src/client-keystore.jks", "changeit", "changeit"
        );

        Socket socket = sslContext.getSocketFactory().createSocket("localhost", 8888);
                //new Socket( "localhost", 8888 );

        try(BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()))){
            String line;
            while((line=br.readLine())!=null){
                System.out.println("line = " + line);
            }
        }

    }
}
