package lab.task;

import java.security.BasicPermission;

public class OperationPermission extends BasicPermission {
    public OperationPermission(String name) {
        super(name.toUpperCase());
    }
}
