package lab.task;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;

public class TaskMain {

    public static void main(String[] args) throws IOException {

        new URL("http://www.google.com").openConnection().connect();

        String task = "delete";

        boolean performed = performTask(task);
        System.out.println("task performed: " + performed);

    }

    private static boolean performTask(String task){
        SecurityManager securityManager = System.getSecurityManager();
        if(securityManager!=null){
            securityManager.checkPermission(new OperationPermission(task) );
            securityManager.checkPermission(new WeekDayPermission());
        }
        System.out.println("Performing task: " + task);
        return true;
    }
}
