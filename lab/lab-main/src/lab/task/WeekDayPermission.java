package lab.task;

import java.security.BasicPermission;
import java.time.LocalDate;

public class WeekDayPermission extends BasicPermission {
    public WeekDayPermission(String day) {
        super(day);
    }

    public WeekDayPermission(){
        this(LocalDate.now().getDayOfWeek().name());
    }
}
