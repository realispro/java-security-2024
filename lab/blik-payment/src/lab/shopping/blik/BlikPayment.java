package lab.shopping.blik;

import lab.shopping.Payment;

import java.util.Scanner;

public class BlikPayment implements Payment {

    private BlikPayment(){}
    @Override
    public void pay(double value) {
        Scanner scanner = new Scanner(System.in);
        String code = scanner.nextLine();
        if(code.equals("1234")){
            System.out.println("blik payment accepted, amount: " + value);
        } else {
            throw new IllegalArgumentException("incorrect blik code");
        }
    }
}
